/*
 * Copyright (c) CISIAD, UNED, Spain,  2019. Licensed under the GPLv3 licence
 * Unless required by applicable law or agreed to in writing,
 * this code is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.hardTesting;

import org.openmarkov.full.OpenMarkov;

public class HardTesting {

	public static void main(String[] args) {
		OpenMarkov.main(args);

	}
}